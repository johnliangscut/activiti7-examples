# IDEA使用camunda-modeler

### 生成文件Activiti不生效问题

原因:

* 在设置assignee和candidateUsers，candidateGroups时，
  activiti无法解析，只有将其前缀从camunda改为activiti时，才可以使用。
  如："activiti:assignee"
  
### 思路

替换命名空间和标签:
* camunda-> activiti xml/bpmn  and  activiti->camunda xml/bpmn
* 运行ConversionUtil
* [gitee地址](https://gitee.com/comfort/activiti7-examples)